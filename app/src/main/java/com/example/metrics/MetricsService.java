package com.example.metrics;


import com.example.metrics.manager.MetricsManager;

/**
 * MetricsService
 *
 * Metrics service take care of an execution of metrics in the background.
 */
public class MetricsService extends TaskServiceBase {
    private MetricsManager manager;


    @Override
    public void onCreate() {
        super.onCreate();

        .
        .
        .

        while (...)
            manager.add(...);
        }

        manager.start();

        .
        .
        .
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        .
        .
        .

        if(manager!=null) {
            manager.stop();
        }

        .
        .
        .
    }
}
