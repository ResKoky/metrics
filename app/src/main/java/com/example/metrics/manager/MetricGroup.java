package com.example.metrics.manager;

import com.example.metrics.model.Metric;

import java.util.ArrayList;
import java.util.List;

/**
 * MetricGroup
 *
 * Metrics container based on the execution period.
 */
class MetricGroup {
    private Integer identifier;
    private List<Metric> metrics;
    private Long executed;
    private Repeat hRepeat;

    public MetricGroup(Integer identifier) {
        this.identifier = identifier;
        metrics = new ArrayList<Metric>();
        executed = 0L;
    }

    public void add(Metric item) {
        metrics.add(item);
    }

    public Integer getIdentifier() {
        return identifier;
    }

    public List<Metric> getMetrics() {
        return metrics;
    }

    public Repeat getRepeat() {
        return hRepeat;
    }

    public void setRepeat(Repeat repeat) {
        this.hRepeat = repeat;
    }

    public Long getExecuted() {
        return executed;
    }

    public void setExecuted(Long executed) {
        this.executed = executed;
    }
}
