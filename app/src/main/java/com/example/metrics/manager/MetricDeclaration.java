package com.example.metrics.manager;

import com.example.metrics.model.Metric;

/**
 * MetricDeclaration
 *
 * Metric declaration builder is used for configuration of a particular metric
 * that is to be initialized and executed.
 */
class MetricDeclaration {
    private Class<? extends Metric> metric;
    private Integer interval;
    private Boolean isOnce;

    private MetricDeclaration(Class<? extends Metric> metric, Integer interval, Boolean isOnce) {
        this.metric = metric;
        this.interval = interval;
        this.isOnce = isOnce;
    }

    static public MetricDeclaration build(Class<? extends Metric> metric, Integer interval, Boolean isOnce) {
        return new MetricDeclaration(metric, interval, isOnce);
    }

    static public MetricDeclaration build(Class<? extends Metric> metric, Integer interval) {
        return build(metric, interval, false);
    }

    public Class<? extends Metric> getMetric() {
        return metric;
    }

    public Integer getInterval() {
        return interval;
    }

    public Boolean isOnce() {
        return isOnce;
    }
}
