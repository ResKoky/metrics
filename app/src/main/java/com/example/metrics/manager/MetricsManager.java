package com.example.metrics.manager;

import com.example.metrics.model.Metric;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * MetricsManager
 *
 * Manages the selection metrics and
 * operates their running cycle.
 */
public class MetricsManager {
    private static final String TAG = MetricsManager.class.getSimpleName();

    public static final int ONCE    = 0;
    public static final int SECOND  = 1000;
    public static final int MINUTE  = 1000*60;

    private HashMap<Integer, MetricGroup> items;
    private Repeat hRepeat;
    private boolean isCapturing = false;


    public MetricsManager() {
        items = new HashMap<Integer, MetricGroup>();
    }

    /**
     * add()
     *
     * Add a new metric to the selection.
     *
     * @param metric
     * @param interval
     * @return
     */
    public MetricsManager add(Metric metric, Integer interval) {
        MetricGroup group = items.get(interval);
        if (group == null) {
            group = new MetricGroup(interval);
            items.put(interval, group);
        }
        group.add(metric);

        if(isCapturing) {
            restart();
        }

        return this;
    }


    /**
     * remove()
     *
     * Add a metric from the selection.
     *
     * @param identifier
     * @return
     */
    public MetricsManager remove(int identifier) {
        Iterator<Map.Entry<Integer, MetricGroup>> iItems = items.entrySet().iterator();
        while (iItems.hasNext()) {
            Map.Entry<Integer, MetricGroup> item = iItems.next();
            MetricGroup group = item.getValue();
            Iterator<Metric> iMetrics = group.getMetrics().iterator();
            while (iMetrics.hasNext()) {
                Metric metric = iMetrics.next();
                if(metric.getIdentifier()==identifier) {
                    metric.stop();
                    group.getMetrics().remove(metric);
                    return this;
                }
            }
        }
        return this;
    }

    /**
     * has()
     *
     * Check if a metric is in the selection.
     *
     * @param identifier
     * @return
     */
    public boolean has(int identifier) {
        Iterator<Map.Entry<Integer, MetricGroup>> iItems = items.entrySet().iterator();
        while (iItems.hasNext()) {
            Map.Entry<Integer, MetricGroup> item = iItems.next();
            MetricGroup group = item.getValue();
            Iterator<Metric> iMetrics = group.getMetrics().iterator();
            while (iMetrics.hasNext()) {
                Metric metric = iMetrics.next();
                if(metric.getIdentifier()==identifier) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * start()
     *
     * Starts capturing the selected metrics.
     */
    public void start() {
        for (final Map.Entry<Integer, MetricGroup> entry: items.entrySet()) {
            final MetricGroup group = entry.getValue();
            if (group.getIdentifier() == MetricsManager.ONCE) {
                switch (entry.getKey()) {
                    case ONCE:
                        capture(group.getIdentifier(), group.getMetrics());
                        break;
                    default:
                        throw new IllegalStateException(
                            TAG + ": Can't run metrics with undefined interval: " + entry.getKey());
                }
            }
        }

        if(hRepeat!=null) {
            hRepeat.cancel();
            hRepeat = null;
        }
        hRepeat = Repeat.build(500, new Runnable() {
            public void run() {
                for (Map.Entry<Integer, MetricGroup> entry: items.entrySet()) {
                    final MetricGroup group = entry.getValue();
                    if (group.getIdentifier() > MetricsManager.ONCE) {
                        if(group.getExecuted() + group.getIdentifier() <= Time.getTime()) {
                            group.setExecuted(Time.getTime());
                            capture(group.getIdentifier(), group.getMetrics());
                        }
                    }
                }
            }
        });

        isCapturing = true;
    }

    /**
     * stop()
     *
     * Stops capturing the selected metrics and clean the callbacks.
     */
    public void stop() {
        if(hRepeat!=null) {
            hRepeat.cancel();
            hRepeat = null;
        }

        if(items!=null && !items.isEmpty()) {
            Iterator<Map.Entry<Integer, MetricGroup>> iItems = items.entrySet().iterator();
            while (iItems.hasNext()) {
                Map.Entry<Integer, MetricGroup> item = iItems.next();
                MetricGroup group = item.getValue();
                Iterator<Metric> iMetrics = group.getMetrics().iterator();
                while (iMetrics.hasNext()) {
                    Metric metric = iMetrics.next();
                    if(metric.isRunning()) {
                        metric.stop();
                    }
                }
            }
        }

        isCapturing = false;
    }

    /**
     * restart()
     *
     * Restarts metrics capturing.
     */
    public void restart() {
        if(isCapturing) {
            stop();
        }
        if(!isCapturing) {
            start();
        }
    }

    /**
     * isRunning()
     *
     * Check if the metric is running.
     *
     * @param identifier
     * @return
     */
    public boolean isRunning(int identifier) {
        Iterator<Map.Entry<Integer, MetricGroup>> iItems = items.entrySet().iterator();
        while (iItems.hasNext()) {
            Map.Entry<Integer, MetricGroup> item = iItems.next();
            MetricGroup group = item.getValue();
            Iterator<Metric> iMetrics = group.getMetrics().iterator();
            while (iMetrics.hasNext()) {
                Metric metric = iMetrics.next();
                if(metric.getIdentifier()==identifier) {
                    return metric.isRunning();
                }
            }
        }

        return false;
    }

    /**
     * getRunning()
     *
     * Get the list of running metrics.
     *
     * @return
     */
    public List<Metric> getRunning() {
        List<Metric> list = new ArrayList<>();
        Iterator<Map.Entry<Integer, MetricGroup>> iItems = items.entrySet().iterator();
        while (iItems.hasNext()) {
            Map.Entry<Integer, MetricGroup> item = iItems.next();
            MetricGroup group = item.getValue();
            Iterator<Metric> iMetrics = group.getMetrics().iterator();
            while (iMetrics.hasNext()) {
                Metric metric = iMetrics.next();
                if(metric.isRunning()) {
                    list.add(metric);
                }
            }
        }
        return list;
    }

    /**
     * capture()
     *
     * Perform capture.
     *
     * @param interval
     * @param metrics
     */
    private void capture(Integer interval, List<Metric> metrics) {
        if(metrics!=null && !metrics.isEmpty()) {
            Iterator<Metric> iMetrics = metrics.iterator();
            while (iMetrics.hasNext()) {
                Metric metric = iMetrics.next();
                try {
                    metric.run();
                } catch (Exception e) {
                    Debug.getInstance().exception(e, metric);
                }
            }
        }
    }
}