package com.example.metrics.model;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 * Headphones
 *
 * Headphones metric captures presence of a headphones connector or connection via Bluetooth.
 */
public class HeadphonesMetric extends Metric {
    private static final String TAG = HeadphonesMetric.class.getSimpleName();
    public static final int IDENTIFIER = 258;

    private Boolean isConnectedLast;

    private WiredHeadsetBroadcastReceiver wiredHeadsetReceiver;

    private BluetoothHeadset bluetoothHeadset;
    private BluetoothStateBroadcastReceiver bluetoothStateReceiver;
    private BluetoothHeadphonesListener bluetoothHeadsetListener;


    public HeadphonesMetric(Context context, Database database, Integer delay) {
        super(context, database, delay);

        bluetoothHeadsetListener = new BluetoothHeadphonesListener();
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "Headphones";
    }

    @Override
    public void run() {
        setRunning(true);

        // Wired headset
        if (wiredHeadsetReceiver == null) {
            getContext().registerReceiver(wiredHeadsetReceiver = new WiredHeadsetBroadcastReceiver(),
                    new IntentFilter(Intent.ACTION_HEADSET_PLUG));
        }

        // Bluetooth headset
        if (bluetoothStateReceiver == null) {
            getContext().registerReceiver(bluetoothStateReceiver = new BluetoothStateBroadcastReceiver(),
                    new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        }

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.getProfileProxy(getContext(), bluetoothHeadsetListener, BluetoothProfile.HEADSET);
        }
    }

    @Override
    public void stop() {
        if (wiredHeadsetReceiver != null) {
            getContext().unregisterReceiver(wiredHeadsetReceiver);
            wiredHeadsetReceiver = null;
        }

        if (bluetoothStateReceiver != null) {
            getContext().unregisterReceiver(bluetoothStateReceiver);
            bluetoothStateReceiver = null;
        }

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null) {
            bluetoothAdapter.closeProfileProxy(BluetoothProfile.HEADSET, bluetoothHeadset);
        }

        setRunning(false);
    }

    @Override
    public long save(Object... params) {
        Boolean isConnected = (Boolean) params[0];
        if (isConnectedLast != isConnected) {
            isConnectedLast = isConnected;
            try {
                Debug.getInstance().log(TAG, isConnected.toString());
                return getDatabase().getHeadphonesRepository().insert(new HeadphonesEntity(
                    Time.getTime(),
                    isConnected
                ));
            } catch (Exception e) {
                Debug.getInstance().exception(e);
                return -1;
            }
        } else {
            return -1;
        }
    }


    /**
     * BluetoothHeadphonesListener
     *
     * Receives connection states from a headset connected via Bluetooth.
     */
    private class BluetoothHeadphonesListener implements BluetoothProfile.ServiceListener {
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            if (profile == BluetoothProfile.HEADSET) {
                bluetoothHeadset = (BluetoothHeadset) proxy;
                save(!bluetoothHeadset.getConnectedDevices().isEmpty());
            }
        }

        public void onServiceDisconnected(int profile) {
            if (profile == BluetoothProfile.HEADSET) {
                save(false);
                bluetoothHeadset = null;
            }
        }
    }

    /**
     * BluetoothStateBroadcastReceiver
     *
     * Receives communication from a headset connected via Bluetooth.
     */
    private class BluetoothStateBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF);
                if (state == BluetoothAdapter.STATE_ON || state == BluetoothAdapter.STATE_OFF) {
                    Boolean isOn = state == BluetoothAdapter.STATE_ON;
                    Debug.getInstance().log(TAG, "bluetooth: " + (isOn ? "on" : "off"));
                }
            }
        }
    }

    /**
     * WiredHeadsetBroadcastReceiver
     *
     * Receives communication from a headset connected via wire.
     */
    private class WiredHeadsetBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra("state", 4);
                boolean isConnected = state == 1;
                save(isConnected);
            }
        }
    }
}
