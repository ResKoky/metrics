package com.example.metrics.model;

/**
 * Identifiable
 *
 * Allows the metric to have a specific identity
 */
public interface Identifiable {
    int getIdentifier();
    String getName();
}
