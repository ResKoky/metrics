package com.example.metrics.model;

/**
 * Persistent
 *
 * Allows the metric to be stored into the database
 */
public interface Persistent {
    public long save(Object... params);
}
